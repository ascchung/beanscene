# BeanScene

**BeanScene** is a Django-based web application where coffee enthusiasts can submit, review, and manage coffee bean entries. The app allows users to rate coffee beans, share their thoughts, and explore reviews from the community.

## Features

- **Coffee Bean Submission**: Users can submit coffee bean entries with details such as name, origin, and flavor profile.
- **Reviews & Ratings**: Users can rate coffee beans from 1 to 5 stars and leave detailed reviews.
- **User Authentication**: Only authenticated users can submit and review coffee beans.
- **Review Moderation**: Reviews can be approved or rejected through a moderation system.
- **Responsive Design**: Accessible on mobile, tablet, and desktop devices.

## Technologies Used

- **Backend**: Django (Python)
- **Frontend**: HTML, CSS (Bootstrap, Tailwind)
- **Database**: SQLite (default, but configurable to PostgreSQL or others)
- **Deployment**: Docker, Gunicorn
- **Other Tools**: Django ORM, Django Forms, Aggregation (Avg), Authentication System

## Installation & Setup

### Prerequisites

Ensure you have the following installed:

- Python 3.x
- Docker (optional for deployment)

### Local Development Setup

1. **Clone the repository**:

   ```bash
   git clone https://github.com/yourusername/beanscene.git
   cd beanscene
   ```

2. **Set up a virtual environment** (recommended):

   ```bash
   python -m venv venv
   source venv/bin/activate  # On Windows: `venv\Scripts\activate`
   ```

3. **Install dependencies**:

   ```bash
   pip install -r requirements.txt
   ```

4. **Set up the database**:

   - By default, SQLite is used. If desired, modify `settings.py` for other databases like PostgreSQL.
   - Apply migrations:

   ```bash
   python manage.py migrate
   ```

5. **Create a superuser** to access the admin panel:

   ```bash
   python manage.py createsuperuser
   ```

6. **Run the development server**:

   ```bash
   python manage.py runserver
   ```

7. **Access the app**: Open `http://127.0.0.1:8000` in your browser.

### Docker Setup (Optional)

1. **Build the Docker image**:

   ```bash
   docker build -t beanscene .
   ```

2. **Run the Docker container**:

   ```bash
   docker run -d -p 8000:8000 beanscene
   ```

3. **Access the app**: Open `http://localhost:8000` in your browser.

## Usage

### Submitting Coffee Beans

- Authenticated users can navigate to the "Submit Coffee Bean" page and fill out details such as name, origin, flavor profile, and upload a picture.

### Reviewing Coffee Beans

- Users can rate and review coffee beans on a 1-5 star scale. The average rating for each coffee bean is displayed on the listing page.

### Admin Panel

- The Django admin panel can be accessed at `/admin` to manage users, coffee beans, and reviews.

## Application Structure

```
/accounts        - Handles user authentication and profiles.
/coffee_beans    - Manages coffee bean submissions and listing.
/reviews         - Handles user reviews and ratings.
/templates       - HTML templates for rendering frontend pages.
/static          - CSS, JavaScript, and images.
/manage.py       - Django's project management script.
/Dockerfile      - Docker configuration for deployment.
```
