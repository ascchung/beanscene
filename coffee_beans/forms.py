from django.forms import ModelForm
from reviews.models import CoffeeBean


class CoffeeBeanForm(ModelForm):
    class Meta:
        model = CoffeeBean
        fields = [
            "name",
            "origin",
            "flavor_profile",
            "picture"
        ]
