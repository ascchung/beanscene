from django.contrib import admin
from coffee_beans.models import CoffeeBean
# Register your models here.
@admin.register(CoffeeBean)
class CoffeeBeanAdmin(admin.ModelAdmin):
    list_display = [
        "name",
        "origin",
        "flavor_profile",
        "picture"
    ]
