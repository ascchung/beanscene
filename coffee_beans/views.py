from django.shortcuts import render, get_object_or_404, redirect
from coffee_beans.models import CoffeeBean
from reviews.models import Review
from coffee_beans.forms import CoffeeBeanForm
from django.contrib.auth.decorators import login_required
from django.db.models import Avg


# Create your views here.
def show_coffee(request, id):
    coffee_bean = get_object_or_404(CoffeeBean, id=id)
    reviews = Review.objects.filter(coffee_bean=coffee_bean)
    context = {
        "coffee_bean": coffee_bean,
        "reviews": reviews,
    }
    return render(request, "coffee/detail.html", context)

def list_coffee(request):
    coffee_beans = CoffeeBean.objects.annotate(avg_rating=Avg('reviews__rating'))
    context = {
        "list_coffee": coffee_beans,
    }
    return render(request, "coffee/list.html", context)

@login_required
def add_coffee(request):
    if request.method == "POST":
        form = CoffeeBeanForm(request.POST)
        if form.is_valid():
            new_coffee_bean = form.save(commit=False)
            new_coffee_bean.reviewer = request.user
            new_coffee_bean.save()
            return redirect("list_coffee")
    else:
        form = CoffeeBeanForm()
    context = {
        "CoffeeBeanForm": form,
    }
    return render(request, "coffee/create.html", context)
