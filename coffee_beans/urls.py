from django.urls import path
from coffee_beans.views import (
    list_coffee,
    show_coffee,
    add_coffee,
)

urlpatterns = [
    path("", list_coffee, name="list_coffee"),
    path("<int:id>/", show_coffee, name="show_coffee"),
    path("add_coffee/", add_coffee, name="add_coffee"),
]
