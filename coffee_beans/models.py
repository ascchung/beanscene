from django.db import models

class CoffeeBean(models.Model):
    name = models.CharField(max_length=200)
    origin = models.CharField(max_length=200)
    flavor_profile = models.TextField()
    picture = models.URLField(default="URL only")

    def __str__(self):
        return self.name
