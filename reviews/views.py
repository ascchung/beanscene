from django.shortcuts import render, get_object_or_404, redirect
from coffee_beans.models import CoffeeBean
from reviews.models import Review
from reviews.forms import ReviewForm
from django.contrib.auth.decorators import login_required


# Create your views here.

def add_review(request, id):
    coffee_bean = get_object_or_404(CoffeeBean, id=id)
    if request.method == "POST":
        form = ReviewForm(request.POST)
        if form.is_valid():
            review = form.save(False)
            review.coffee_bean = coffee_bean
            review.reviewer = request.user
            review.save()
            return redirect("show_coffee", id=id)
    else:
        form = ReviewForm()
    context = {
        "ReviewForm": form,
    }

    return render(request, "reviews/create.html", context)

@login_required
def user_reviews(request):
    user_reviews = Review.objects.filter(reviewer=request.user)
    context = {
        "user_reviews": user_reviews,
    }
    return render(request, "reviews/user_reviews.html", context)

def edit_review(request, pk):
    review = get_object_or_404(Review, id=pk)
    if request.method == "POST":
        form = ReviewForm(request.POST, instance=review)
        if form.is_valid():
            form.save()
            return redirect("show_coffee", id=pk)
    else:
        form = ReviewForm(instance=review)
    context = {
        "ReviewForm": form,
    }
    return render(request, "reviews/edit.html", context)
