from django.db import models
from django.conf import settings
from coffee_beans.models import CoffeeBean


# Create your models here.
class Review(models.Model):
    rating_choices = [
        (1, '1 Star'),
        (2, '2 Stars'),
        (3, '3 Stars'),
        (4, '4 Stars'),
        (5, '5 Stars'),
    ]

    rating = models.IntegerField(choices=rating_choices)
    review = models.TextField(default='Write your review here')
    review_date = models.DateTimeField(auto_now_add=True)

    reviewer = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.PROTECT,
    )

    coffee_bean = models.ForeignKey(
        CoffeeBean,
        related_name="reviews",
        on_delete=models.PROTECT,
    )

    class Meta:
        unique_together = ['coffee_bean', 'reviewer']

    def __str__(self):
        return f"{self.coffee_bean.name} - {self.reviewer}"

    def approve(self):
        self.is_approved = True
        self.save()

    def reject(self):
        self.is_approved = False
        self.save()
