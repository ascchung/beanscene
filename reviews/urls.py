from django.urls import path
from reviews.views import (
    add_review,
    user_reviews,
    edit_review
)

urlpatterns = [
    path("<int:id>/add_review/", add_review, name="add_review"),
    path("user_reviews/", user_reviews, name="user_reviews"),
    path("<int:pk>/edit_review/", edit_review, name="edit_review"),

]
